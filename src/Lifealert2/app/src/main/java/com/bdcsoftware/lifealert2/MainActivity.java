package com.bdcsoftware.lifealert2;

import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.hardware.Camera;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorManager;
//import android.hardware.usb.UsbDeviceConnection;
//import android.hardware.usb.UsbManager;
//import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.view.InputDevice;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.atap.tangoservice.Tango;
import com.google.atap.tangoservice.Tango.OnTangoUpdateListener;
import com.google.atap.tangoservice.TangoConfig;
import com.google.atap.tangoservice.TangoCoordinateFramePair;
import com.google.atap.tangoservice.TangoErrorException;
import com.google.atap.tangoservice.TangoEvent;
import com.google.atap.tangoservice.TangoOutOfDateException;
import com.google.atap.tangoservice.TangoPoseData;
import com.google.atap.tangoservice.TangoXyzIjData;

import android.app.Activity;
import android.os.Bundle;

import android.annotation.SuppressLint;

import java.util.ArrayList;


//import com.bdcsoftware.packetserial.Packet;
import com.bdcsoftware.packetserial.PacketSerial;
import com.projecttango.rajawali.Pose;
import com.projecttango.rajawali.ScenePoseCalculator;
//import com.bdcsoftware.packetserial.SerialPortNotOpenException;
//import com.bdcsoftware.packetserial.SimpleDualMotorPacket;
//import android.app.Activity;
//import android.os.Bundle;
//import android.util.Log;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.FrameLayout;
//import android.widget.ImageButton;
//import android.app.Activity;
//import android.hardware.Sensor;
//import android.hardware.SensorEvent;
//import android.hardware.SensorEventListener;
//import android.hardware.SensorManager;
//import android.os.Bundle;
//import android.widget.TextView;

import org.rajawali3d.math.Quaternion;
import org.rajawali3d.math.vector.Vector3;

import java.io.IOException;
//import java.util.List;
//import java.util.Locale;
//
//import android.app.Activity;
//import android.content.Context;
//import android.location.Address;
//import android.location.Criteria;
//import android.location.Geocoder;
//import android.location.Location;
//import android.location.LocationListener;
//import android.location.LocationManager;
//import android.os.Bundle;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;

import com.google.atap.tangoservice.TangoPoseData;

import android.content.Context;
import android.graphics.Color;
import android.view.MotionEvent;

import org.rajawali3d.math.Quaternion;
import org.rajawali3d.math.vector.Vector3;
import org.rajawali3d.renderer.RajawaliRenderer;

import com.projecttango.rajawali.Pose;
import com.projecttango.rajawali.ScenePoseCalculator;
import com.projecttango.rajawali.renderables.Grid;



public class MainActivity extends Activity {
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String sTranslationFormat = "Translation: %f, %f, %f";
    private static final String sRotationFormat = "Rotation: %f, %f, %f, %f";

    private static final int SECS_TO_MILLISECS = 1000;
    private static final double UPDATE_INTERVAL_MS = 100.0;

    private double mPreviousTimeStamp;
    private double mTimeToNextUpdate = UPDATE_INTERVAL_MS;

    private TextView mTranslationTextView;
    private TextView mRotationTextView;

    private Tango mTango;
    private TangoConfig mConfig;
    private boolean mIsTangoServiceConnected;

    private SensorManager mSensorManager;
    private Sensor mCompass;
    private TextView mTextView;
    private Map myMap;

    private Pose mDevicePose = new Pose(Vector3.ZERO, Quaternion.getIdentity());
    private boolean mPoseUpdated = false;

    private BaseRover myRover;
    //private View map;
    private PacketSerial packetSerial;
    private BroadcastReceiver mUSBDetachedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("BroadcastReciever", "Broadcast");
        }
    };
    /**
     * On create is the main startup method. It only runs once per activity lifecycle
     * @param savedInstanceState
     */
    // Tango: 192.168.0.103
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
///////////////////////////////////////////////////////

///////////////////////////////////////////////////////


        mTranslationTextView = (TextView) findViewById(R.id.translation_text_view);
        mRotationTextView = (TextView) findViewById(R.id.rotation_text_view);
        myMap = (Map) findViewById(R.id.myMap);

        //instantiate this as TestRover if you aren't using the robot
        //instantiate this as Rover if you are using the robot
        myRover = new Rover();

        // Instantiate Tango client
        mTango = new Tango(this);

        // Set up Tango configuration for motion tracking
        // If you want to use other APIs, add more appropriate to the config
        // like: mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_DEPTH, true)
        mConfig = mTango.getConfig(TangoConfig.CONFIG_TYPE_CURRENT);
        mConfig.putBoolean(TangoConfig.KEY_BOOLEAN_MOTIONTRACKING, true);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mCompass = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
        mTextView = (TextView) findViewById(R.id.tvSensor);


        packetSerial = PacketSerial.getInstance();
        packetSerial.setContext(this);


        Button snakeButton = (Button)findViewById(R.id.snakebutton);
        snakeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myRover.isAutonomous()) {
                    Log.d("SnakeButton", "The snake is dead.");
                    myRover.killAI();
                } else {
                    Log.d("SnakeButton", "The Snake Made A Hiss.");
                    myRover.runAI();
                }

            }
        });

        




    }

    public BaseRover getMyRover() {
        return myRover;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        boolean handled = false;

        //if (event.getRepeatCount() == 0) {
        Log.d("LogPush", "I was pushed KEY:" + keyCode);
        if (keyCode == 19) {
            Log.d("LogPush", "UP!" + keyCode);
        }
        if (keyCode == 20) {
            Log.d("LogPush", "DOWN!" + keyCode);
        }
        if (keyCode == 21) {
            Log.d("LogPush", "LEFT!" + keyCode);
        }
        if (keyCode == 22) {
            Log.d("LogPush", "RIGHT!!" + keyCode);
        }
        if (keyCode == 102) {
            Log.d("LogPush", "STOP!!" + keyCode);
        }
        if(keyCode == 4) {
            Log.d("LogPush", "SnakeButton: " + keyCode);
            if(myRover.isAutonomous()) {
                Log.d("SnakeButton", "The snake is dead.");
                myRover.killAI();
            } else {
                Log.d("SnakeButton", "The Snake Made A Hiss.");
                myRover.runAI();
            }
        }

        return handled;
    }

    @Override
    protected void onResume() {
       super.onResume();
        IntentFilter intent = new IntentFilter("android.hardware.usb.action.USB_DEVICE_DETACHED");
        registerReceiver(mUSBDetachedReceiver, intent);
        // Lock the Tango configuration and reconnect to the service each time
        // the app
        // is brought to the foreground.
        if (!mIsTangoServiceConnected) {
            try {
                setTangoListeners();
            } catch (TangoErrorException e) {
                Toast.makeText(this, "Tango Error! Restart the app!",
                        Toast.LENGTH_SHORT).show();
            }
            try {
                mTango.connect(mConfig);
                mIsTangoServiceConnected = true;
            } catch (TangoOutOfDateException e) {
                Toast.makeText(getApplicationContext(),
                        "Tango Service out of date!", Toast.LENGTH_SHORT)
                        .show();
            } catch (TangoErrorException e) {
                Toast.makeText(getApplicationContext(),
                        "Tango Error! Restart the app!", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        //PacketSerial//
        try {
            //mSensorManager.registerListener(myRover, mCompass, SensorManager.SENSOR_DELAY_NORMAL);
            packetSerial.openSerialPort();


        } catch (IOException e) {
            e.printStackTrace();
        }
        //////////
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(mUSBDetachedReceiver);
        // When the app is pushed to the background, unlock the Tango
        // configuration and disconnect
        // from the service so that other apps will behave properly.
        try {
            mTango.disconnect();
            mIsTangoServiceConnected = false;
        } catch (TangoErrorException e) {
            Toast.makeText(getApplicationContext(), "Tango Error!",
                    Toast.LENGTH_SHORT).show();
        }
        try {
            //mSensorManager.unregisterListener(myRover);
            packetSerial.closeSerialPort();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    private void setTangoListeners() {
        // Select coordinate frame pairs
        ArrayList<TangoCoordinateFramePair> framePairs = new ArrayList<TangoCoordinateFramePair>();
        framePairs.add(new TangoCoordinateFramePair(
                TangoPoseData.COORDINATE_FRAME_START_OF_SERVICE,
                TangoPoseData.COORDINATE_FRAME_DEVICE));

        // Add a listener for Tango pose data
        mTango.connectListener(framePairs, new OnTangoUpdateListener() {

            @SuppressLint("DefaultLocale")
            @Override
            public void onPoseAvailable(TangoPoseData pose) {
                // Format Translation and Rotation data
                final String translationMsg = String.format(sTranslationFormat,
                        pose.translation[0], pose.translation[1],
                        pose.translation[2]);
                final String rotationMsg = String.format(sRotationFormat,
                        pose.rotation[0], pose.rotation[1], pose.rotation[2],
                        pose.rotation[3]);

                // Output to LogCat
                String logMsg = translationMsg + " | " + rotationMsg;
                //Log.i(TAG, logMsg);

                final double deltaTime = (pose.timestamp - mPreviousTimeStamp)
                        * SECS_TO_MILLISECS;
                mPreviousTimeStamp = pose.timestamp;
                mTimeToNextUpdate -= deltaTime;

                myRover.setTranslationTango(pose.translation);
                myRover.setRotationTango(pose.rotation);

                // Throttle updates to the UI based on UPDATE_INTERVAL_MS.
                if (mTimeToNextUpdate < 0.0) {
                    mTimeToNextUpdate = UPDATE_INTERVAL_MS;

                    // Display data in TextViews. This must be done inside a
                    // runOnUiThread call because
                    // it affects the UI, which will cause an error if performed
                    // from the Tango
                    // service thread

                    ////////////////////////////

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mRotationTextView.setText(rotationMsg);
                            mTranslationTextView.setText(translationMsg);
                            myMap.setMap(myRover.getMap());
                            myMap.invalidate();
                        }
                    });


                    ////////////////////////////
                }
            }

            @Override
            public void onXyzIjAvailable(TangoXyzIjData arg0) {
                // Ignoring XyzIj data
            }

            @Override
            public void onTangoEvent(TangoEvent arg0) {
                // Ignoring TangoEvents
            }

            @Override
            public void onFrameAvailable(int arg0) {
                // Ignoring onFrameAvailable Events

            }

        });
    }



    @Override
    public boolean onGenericMotionEvent(MotionEvent event) {
        // Check that the event came from a game controller
        if ((event.getSource() & InputDevice.SOURCE_JOYSTICK) ==
                InputDevice.SOURCE_JOYSTICK &&
                event.getAction() == MotionEvent.ACTION_MOVE) {

            // Process all historical movement samples in the batch
            final int historySize = event.getHistorySize();

            // Process the movements starting from the
            // earliest historical position in the batch
            for (int i = 0; i < historySize; i++) {
                // Process the event at historical position i
                processJoystickInput(event, i);
            }

            // Process the current movement sample in the batch (position -1)
            processJoystickInput(event, -1);
            return true;
        }
        return super.onGenericMotionEvent(event);
    }

    /*
     * I'm not gonna mess with this, I'll take your word for it that it works
     * However, I don't think we need it because of how the processJoystickInput method works
    */
    private static float getCenteredAxis(MotionEvent event,
                                         InputDevice device, int axis, int historyPos) {
        final InputDevice.MotionRange range =
                device.getMotionRange(axis, event.getSource());

        // A joystick at rest does not always report an absolute position of
        // (0,0). Use the getFlat() method to determine the range of values
        // bounding the joystick axis center.
        if (range != null) {
            final float flat = range.getFlat();
            final float value =
                    historyPos < 0 ? event.getAxisValue(axis):
                            event.getHistoricalAxisValue(axis, historyPos);

            // Ignore axis values that are within the 'flat' region of the
            // joystick axis center.
            if (Math.abs(value) > flat) {
                return value;
            }
        }
        return 0;
    }
    //Making the joystick work, this allows X and Y Axis controls on the stick to be detected.
    //This also enables us to then add keyevents using Dennys math to check the ammount of force each wheel must
    //turn.
    //   [LStick]:Forward and backwards movement.    [Rstick]: left and right turning.
    //
    private void processJoystickInput(MotionEvent event,
                                      int historyPos) {
        InputDevice mInputDevice = event.getDevice();

        float x = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_Z, historyPos);
        float y = getCenteredAxis(event, mInputDevice,
                MotionEvent.AXIS_Y, historyPos);
        Log.d("LogPush","AnalogX:"+x);
        Log.d("LogPush","AnalogY:"+y);

        // Channel 1 STOP:64 Channel 2 STOP:192
        int rightMotor = BaseRover.RIGHT_STOP, leftMotor = BaseRover.LEFT_STOP;

        if(Math.round(Math.abs(63*y)) > 0) {
            rightMotor -= Math.round(63*y);
            leftMotor -= Math.round(63*y);
            if (x > 0) {
                rightMotor -= Math.round(63 * x);
            } else if (x < 0) {
                leftMotor += Math.round(63 * x);
            }
        } else {
            rightMotor -= Math.round(63*x);
            leftMotor += Math.round(63*x);
        }

        //if(leftMotor > 127 || rightMotor > 256)
        //{
            Log.d("Motor", "Left:" + leftMotor);
            Log.d("Motor", "Right:" + rightMotor);
        //}

        myRover.sendPayload(leftMotor, rightMotor); //Send Payload LMotor And RMotor
    }

//EndBracketOfItAll
}
/*
     * I felt like there should be one more bracket, just cause.
    } the true EndBracketOfItAll
*/

