package com.bdcsoftware.lifealert2;

import android.util.Log;

/**
 * Created by rafaelszuminski on 2/26/16.
 */
public class TestRover extends BaseRover {

    public TestRover() {
        super();
    }
/*  LOG 2/28/16   */
/*
 []FinishClose
    []Finish Map


 */



    //this should only be called for manual control input
    public void sendPayload(int leftMotor, int rightMotor) {
        if(!isAutonomous()) {
            this.leftMotor = leftMotor;
            this.rightMotor = rightMotor;
            sendPayload();
        }
    }

}
