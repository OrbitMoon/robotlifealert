package com.bdcsoftware.lifealert2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.google.atap.tangoservice.TangoPoseData;
import com.projecttango.rajawali.Pose;
import com.projecttango.rajawali.ScenePoseCalculator;
import com.projecttango.rajawali.TouchViewHandler;
import com.projecttango.rajawali.renderables.FrustumAxes;
import com.projecttango.rajawali.renderables.Grid;
import com.projecttango.rajawali.renderables.Trajectory;

import org.rajawali3d.math.Quaternion;
import org.rajawali3d.math.vector.Vector3;
import org.rajawali3d.renderer.RajawaliRenderer;

public class Map extends View {
    private int[][] map;
    private Paint black = new Paint();
    private Context mContext;

    public Map(Context context) {
        super(context);
        mContext = context;
    }

    public Map(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        black.setStyle(Paint.Style.FILL);
    }

    public int[][] getMap() {
        return map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);


//        int x = getWidth();
//        int y = getHeight();
//        int radius;
//        radius = 100;
//        Paint paint = new Paint();
//        paint.setStyle(Paint.Style.FILL);
//        paint.setColor(Color.RED);
//        canvas.drawPaint(paint);
//        Use Color.parseColor to define HTML colors
//        paint.setColor(Color.RED);
//        canvas.drawCircle(x / 2, y / 2, radius, paint);
//        Log.d("TEST", "Map.onDraw()");
//
//
//        canvas.drawColor(Color.RED);
//        for(int i = 0; i < map.length; i++) {
//            for(int j = 0; j < map[i].length; j++) {
//                if(map[i][j] == 1) {
//                    canvas.drawRect(i*getWidth(), j*getHeight(),
//                            (i+1)*getWidth(), (j+1)*getHeight(), black);
//                }
//            }
//        }



        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.RED);


        int width = getScreenResolution().x;

        //This Draws the top Line, Keep this for it just makes sure that we can draw
        for (int x = 0; x < width ; x = x + 8) {
            canvas.drawCircle(x, 250, 2, paint);
        }
//This if the Map is not Null then draw the cricle of Pos i and j at size 4
        if (map != null) {
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[i].length; j++) {
                    if (map[i][j] == 1) {
                        canvas.drawCircle(1900*i/1000, 1200*j/1000, 2, paint);
                    }
                }
            }
        } else {
            Log.d("TEST", "map is null");
            //This runs in a loop remember, so once you let the tablet initialize then it will loop
            //back and you will no longer have this error.
        }
    }

    private Point getScreenResolution(){
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size;
    }



//    public Map(Context context) {
//        super(context);
//        int x = 50;
//        int y = 50;
//        int sideLength = 200;
//
//        // create a rectangle that we'll draw later
//        rectangle = new Rect(x, y, sideLength, sideLength);
//
//        // create the Paint and set its color
//        paint = new Paint();
//        paint.setColor(Color.GRAY);
//    }


}