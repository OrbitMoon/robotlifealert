package com.bdcsoftware.lifealert2;

import android.os.AsyncTask;
import android.util.Log;


/**
 * Created by dkfrayne on 2/26/16.
 */
public abstract class BaseRover {
    public static final int RIGHT_STOP = 192;
    public static final int LEFT_STOP = 64; //motor values
    protected boolean autonomous; //this being true is intended for overriding manual control with autonomous
    protected int leftMotor;
    protected int rightMotor; //these need to belong strictly to Rover class if Rover is in charge of motion
    protected double[] translationTango = new double[3];
    protected double[] rotationTango = new double[3];
    Rover.AutonomousAI autoAI;
    private int[][] map;
    private int[] location;
    private boolean out_of_bounds;
    /*
     That moment when Denny writes stupid code and you realise its actually just not finished what a nerd.
     That moment when Raf writes brilliant code and you realize that he's just too good at command c
     3/1/16 -Subliminal Msg Man
    */

    public BaseRover() {
        leftMotor = LEFT_STOP;
        rightMotor = RIGHT_STOP;
        autoAI = new AutonomousAI();
        map = new int[1000][1000];
        location = new int[] {500, 500};
        mapInit();
        out_of_bounds = false;
    }

    private void mapInit() {
        for(int i = 0; i < map.length; i++)
            for(int j = 0; j < map[i].length; j++)
                map[i][j] = 0;
    }
    public double[] getTranslationTango() {
        return translationTango;
    }

    public void setTranslationTango(double[] translationTango) {
        this.translationTango = translationTango;

        location[0] = 500 + (int)Math.round(translationTango[0]*10);
        location[1] = 500 + (int)Math.round(translationTango[1]*10); //this is for testing which axis works
        try {
            map[location[0]][location[1]] = 1;
        } catch(ArrayIndexOutOfBoundsException e) {
            e.printStackTrace();
            Log.d("Rover", "You're outside the map!");
        }
    }
    public int[][] getMap() {
        return map;
    }
    public void setRotationTango(double[] rotationTango) {
        this.rotationTango = rotationTango;
    }

    public void faceDirection(double target) {
    }

    //More Subliminal MSGs
    //Less Subliminal MSGs
    public void returnToMap() {
        double theta = Math.asin((double) location[1]/location[0]);
        double target = Azimuth.compassAddition(theta, 180.0);
        faceDirection(target);
    }

    public boolean isAutonomous() {
        return autonomous;
    }

    public void setAutonomous(boolean autonomous) {
        Log.d("AutoAI", "autonomus:" + autonomous);
        this.autonomous = autonomous;
    }

    public abstract void sendPayload(int leftMotor, int rightMotor);

    protected void sendPayload() {
        Log.d("Payload", "Right: " + rightMotor + ", Left: " + leftMotor);
    }

    public void runAI() {
        autoAI = new AutonomousAI();
        autoAI.execute();
    }

    public void killAI() {
        setAutonomous(false);
    }

    protected double distance(double[] coord1, double[] coord2) {
        int dim = coord1.length < coord2.length ? coord1.length : coord2.length;
        double sum = 0;
        for(int i = 0; i < dim; i++) {
            sum += Math.pow(coord1[i] - coord2[i], 2);
        }
        return Math.sqrt(sum);
    }

    protected class AutonomousAI extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            setAutonomous(false);
            Log.d("AutonomousAI", "Task finished");
        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.d("AutonomousAI", "Snake method init");
            setAutonomous(true);
            //snake3();
            tangoSnake2(3);
            return null;
        }

        /*
         * This method course corrects the robot auto-magically using the android compass
         * bearing and some clever geometry from the compareTo method of the azimuth class
         */

        // Really, 'Auto-Magically'
        // Yes, 'Auto-Magically'. you think of something better.

//        private void updateMotorsWithTargetBearing(Azimuth targetDirection) {
//            if(Math.abs(azimuth.compareTo(targetDirection)) > 5) {
//                rightMotor += Math.round(azimuth.compareTo(targetDirection))/5;
//                leftMotor -= Math.round(azimuth.compareTo(targetDirection))/5;
//                sendPayload();
//                Log.d("AutonomousAI", "updateMotors: L" + leftMotor + " R" + rightMotor);
//            }
//        }

        public void moveForward2Secs() {
            long startTime = System.currentTimeMillis();
            rightMotor = RIGHT_STOP + 55;
            leftMotor = LEFT_STOP + 55;
            Log.d("AutoAI", "Moving Forward");
            //sendPayload();
            while(true) {
                Log.d("AutoAI", "inside loop");
                Thread.yield();
                if(!autonomous) {
                    break;
                }
            }
            Log.d("AutoAI", "Stopping");
            rightMotor = RIGHT_STOP;
            leftMotor = LEFT_STOP;
            //sendPayload();
        }

        //This Is The Test It Randomly Works//
        //This is the test that Raf tried to write and Denny swooped in and saved the day with his
        //brilliant logic and mathematical sense//
        public void tangoSnake(double distance){
            //get current tango distance
            //store in private var

            double startTango = translationTango[1];
            double currentTango = startTango;
            Log.d("AutonomousAI", "Tango Values Set");
            leftMotor = LEFT_STOP + 55;
            rightMotor = RIGHT_STOP + 55;
            //start motors forward
            sendPayload();

            while (Math.abs(currentTango-startTango) < distance){
                currentTango = translationTango[1];
                try {
                    Log.d("Tango", "You have traveled " + distance);
                } catch(NullPointerException n) {
                    n.printStackTrace();
                }
                if(isCancelled()) break;
            }
            if(Math.abs(currentTango-startTango) < distance) {
                Log.d("Tango", "You traveled " + distance + " meters.");
            }
            rightMotor = RIGHT_STOP;
            leftMotor = LEFT_STOP;
            sendPayload();

            Log.d("AutonomousAI", "Ending Tango Test!");
        }

        public void tangoSnake2(double distance){
            //get current tango distance
            //store in private var

            double[] startTango = translationTango;
            double[] currentTango = startTango;

            // actual distance covered will be 0.0d at first
            double distanceCovered = distance(startTango, currentTango);
            Log.d("AutonomousAI", "Tango Values Set");
            leftMotor = LEFT_STOP + 55;
            rightMotor = RIGHT_STOP + 55;
            //start motors forward
            sendPayload();

            while (distanceCovered < distance){
                currentTango = translationTango;
                distanceCovered = distance(startTango, currentTango);
                Log.d("Tango", "You have traveled " + distanceCovered);
                if(isCancelled()) break;
            }
            Log.d("Tango", "You traveled " + distanceCovered + " of " + distance + " meters.");
            rightMotor = RIGHT_STOP;
            leftMotor = LEFT_STOP;
            sendPayload();

            Log.d("AutonomousAI", "Ending Tango Test!");
        }
    }
}
