package barcode.core;

import java.util.ArrayList;

/***
 * This binarizer uses specific knowledge of the Code39 barcodes
 * to make assumptions about the captured image and it's properties. The 
 * default behavior is to use a localized blackpoint threshold. If the algorithm is
 * unable to calculate a local threshold, it will fall back to use a 
 * limited global histogram.
 * 
 * DO NOT USE THIS BINARIZER FOR OTHER 1D OR 2D BARCODES.
 * @author RSzuminski
 *
 */
public class Code39HistogramBinarizer extends GlobalHistogramBinarizer {

	private int[] lums = null;

	public Code39HistogramBinarizer(LuminanceSource source) {
		super(source);
	}

	@Override
	public BitArray getBlackRow(int y, BitArray row) throws NotFoundException {
		//return getRowFromLimitedHistogram(y, row);
		return getRowFromLocalizedHistogram(y, row);
	}

	private BitArray getRowFromLimitedHistogram(int y, BitArray row) {
		LuminanceSource source = getLuminanceSource();
		int width = source.getWidth();
		if (row == null || row.getSize() < width) {
			row = new BitArray(width);
		} else {
			row.clear();
		}

		byte[] llums = null;
		llums = source.getRow(y, llums);
		// convert to ints and sharpen
		lums = new int[width];

		int left = llums[0] & 0xff;
		int center = llums[1] & 0xff;
		for (int x = 0; x < width - 1; x++) {
			int right = llums[x + 1] & 0xff;
			lums[x] = ((center << 2) - left - right) >> 1;
			left = center;
			center = right;
		}

		int globalblackPoint = estimateSimpleBlackPoint(width);

		for (int x = 1; x < width - 1; x++) {
			if (lums[x] <= globalblackPoint) {
				row.set(x);
			}
		}
		return row;

	}
	
	private BitArray getRowFromLocalizedHistogram(int y, BitArray row){
		LuminanceSource source = getLuminanceSource();
		int width = source.getWidth();
		if (row == null || row.getSize() < width) {
			row = new BitArray(width);
		} else {
			row.clear();
		}

		byte[] llums = null;
		llums = source.getRow(y, llums);
		// convert to ints and sharpen
		lums = new int[width];

		int left = llums[0] & 0xff;
		int center = llums[1] & 0xff;
		for (int x = 0; x < width - 1; x++) {
			int right = llums[x + 1] & 0xff;
			lums[x] = ((center << 2) - left - right) >> 1;
			left = center;
			center = right;
		}

		int globalblackPoint = estimateSimpleBlackPoint(width);

		// get the average width of a thin black line
		ArrayList<Integer> blackBars = new ArrayList<Integer>();
		int blackBar = 0;
		for (int x = 0; x < width; x++) {
			if (lums[x] < globalblackPoint) {
				blackBar++;
			} else {
				if (blackBar > 0) {
					blackBars.add(blackBar);
					blackBar = 0;
				}
			}
		}

		// calculate avg bar width
		int avg = 0;
		for (int i = 0; i < blackBars.size(); i++) {
			avg += blackBars.get(i);

		}
		
		if (blackBars.size() == 0) {
			//we are in trouble, fall back
			return getRowFromLimitedHistogram(y, row);
		}
		
		avg = avg / blackBars.size();

		// remove bars that are double the avg
		for (int i = blackBars.size() - 1; i > -1; i--) {
			if (blackBars.get(i) > avg * 2)
				blackBars.remove(i);
		}

		// find max bar width
		int maxBarWidth = 0;
		for (int i = 0; i < blackBars.size(); i++) {
			if (blackBars.get(i) > maxBarWidth)
				maxBarWidth = blackBars.get(i);
		}

		// count widths of bars
		int widthCounts[] = new int[maxBarWidth + 1];
		for (int i = 0; i < blackBars.size(); i++) {
			widthCounts[blackBars.get(i)]++;
		}

		// the width with the highest count is our thin black bar
		int highCount = 0;
		int highCountPosition = 0;
		for (int i = 0; i < widthCounts.length; i++) {
			if (widthCounts[i] > highCount) {
				highCount = widthCounts[i];
				highCountPosition = i;
			}
		}

		int thin = highCountPosition;

		int chunkToRead = thin * 40;

		int count = 0;
		int blackPoint = 0;
		// process row from the middle in both directions
		for (int x = width / 2; x < width; x++) {

			if ((count % chunkToRead) == 0 || count == 0) {
				blackPoint = getBlackPointForChunk(x, chunkToRead);
			}

			if (lums[x] <= blackPoint) {
				row.set(x);
			}
			count++;
		}

		int negativeChunkToRead = chunkToRead * -1;
		count = 0;
		for (int x = width / 2; x > -1; x--) {
			if ((count % chunkToRead) == 0 || count == 0) {
				blackPoint = getBlackPointForChunk(x, negativeChunkToRead);
			}

			if (lums[x] <= blackPoint) {
				row.set(x);
			}
			count++;
		}

		return row;
	}

	/**
	 * Estimate a simple black point. Process the center 80% of the row.
	 * This ensures most of the time a decent estimation
	 * 
	 * @param width - row width
	 * @return int - average lum value
	 */
	private int estimateSimpleBlackPoint(int width) {
		int avg = 0;
		int spacer = (int) (width * 0.1);
		int count = 0;
		for (int x = spacer; x < width - spacer; x++) {
			count++;
			avg += lums[x];
		}

		avg = avg / count;
		// add some bias towards white
		avg += avg * 10 / 100;
		return avg;
	}

	private int getBlackPointForChunk(int chunkStart, int chunkSize) {
		int avg = 0;

		if (chunkSize > 0) {
			for (int x = chunkStart; (x < chunkStart + chunkSize) && (x < lums.length); x++) {
				avg += lums[x];
			}
		} else {
			for (int x = chunkStart; (x > chunkStart + chunkSize) && (x > -1); x--) {
				avg += lums[x];
			}
		}

		avg = avg / Math.abs(chunkSize);
		return avg;
	}

}
