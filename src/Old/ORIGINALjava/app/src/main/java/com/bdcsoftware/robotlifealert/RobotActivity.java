package com.bdcsoftware.robotlifealert;

public interface RobotActivity {
    void onLogMessage(String response);
    void onLogCommand(String response);
}
