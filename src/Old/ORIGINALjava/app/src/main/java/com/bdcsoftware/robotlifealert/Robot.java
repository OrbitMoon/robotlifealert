package com.bdcsoftware.robotlifealert;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import app.akexorcist.bluetotohspp.library.BluetoothState;
import utils.LogEx;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;


public class Robot extends AsyncTask {
    private RobotActivity mActivity = null;
    private BluetoothSPP bt = null;
    private int mLidarValue = 9999;
    private String currentState = "";
    private String nextState = "";
    private  int mPingValue = 0;

    public boolean mKillRobot = false;

    public Robot(RobotActivity activity){
        mActivity = activity;
    }

    public void setNextState(String value){
        nextState = value;
        mActivity.onLogCommand(value);
    }

    @Override
    protected Object doInBackground(Object[] params) {

        while(!mKillRobot) {
            if (nextState.equalsIgnoreCase(States.STARTUP)) {
                start();
            }


            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.FIND_BUCKET)) {
                findBucket();
            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.AROUND_BUCKET)) {
                aroundBucket();
            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.MOVE_TO_BUCKET)) {
                moveTowardsBucket();
            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.END)) {
                sendCommand(Commands.STOP);
            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.SPIRAL)) {


            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.SNAKE)) {

                snake();
            }

            if (mKillRobot) break;

            if (nextState.equalsIgnoreCase(States.MOVEAWAY)) {
                sendCommand(Commands.BACK);
                safeSleep(2500);
                sendCommand(Commands.RIGHT);
                safeSleep(3000);
                if (nextState == "") {
                    setNextState(States.FIND_BUCKET);
                }
                //moveAwayFromBucket();

            }

        }


        return null;
    }


    public void start(){
        LogEx.d("start()");
        nextState = "";

        //move robot forward for 5 seconds
        goForward(5000);
        sendCommand(Commands.STOP);

        //try to find the bucket
        if (nextState == "") {
            setNextState(States.FIND_BUCKET);
        }

        LogEx.d("end start()");
    }


    public void findBucket(){
        nextState = "";
        //check current lidar distance, if we are close to a bucket, rotate until
        //pointing away from the bucket

        //issue lidar command and wait for response
       // getNewLidarValue();
        getlastPing();

        //start rotating
        sendCommand(Commands.RIGHT);
        LogEx.d("Turning right");
        //read lidar

        int currentLidarValue = mPingValue;
        LogEx.d("Turning right ping value:" + mPingValue);
        while (Math.abs(currentLidarValue - mPingValue) < 99){ //keep turning while the difference is smaller than 200 cm
            //looks like there is a major change in the lidar value
            //break out of the loop

            //getNewLidarValue();
            getlastPing();

        }


        LogEx.d("Found bucket. Stoppin!!!!!");
        sendCommand(Commands.STOP);

        if (nextState == "") {
            setNextState(States.MOVE_TO_BUCKET);
        }
        //moveTowardsBucket();

    }

    public int getNewLidarValue(){
        int temp = mLidarValue;
        while (temp == mLidarValue) {
            //Thread.yield();
            safeSleep(100);
            LogEx.d("Waiting... for lidar value " + mLidarValue);
            sendCommand(Commands.LIDAR);
            //sendCommand(Commands.LIDAR);
        }
        LogEx.d("New lidar value " + mLidarValue);
        return mLidarValue;
    }

    public int getlastPing(){
        int temp = mPingValue;
        int count = 0;
        while (temp == mPingValue || count < 2) {
            //Thread.yield();
            count++;
            safeSleep(300);
           LogEx.d("Waiting... for PING value " + mPingValue);
            sendCommand(Commands.PING);
            //sendCommand(Commands.LIDAR);
        }
        LogEx.d("New PING value " + mPingValue);
        return mPingValue;
    }

    public void moveTowardsBucket(){
        nextState = "";
        //move in the current direction
        //until bucket is reached
        //we will know when bucket is reached when lidar reports less than x inches
        //start moving towards the object
        sendCommand(Commands.FORWARD);

        //keep moving forward UNTIL either lidar is 100 or less
        //or a code has been scanned
        while(true){
            //getNewLidarValue();
            getlastPing();
            if (mPingValue < 100){
                break;
            }
        }
        sendCommand(Commands.STOP);
        if (nextState == "") {
            setNextState(States.AROUND_BUCKET);
        }
        //go into scan bucket function
    }

    public void scanBucket(){
        //drive around the bucket and try to scan the barcode

        //if scan successful
    }
//***************************************************************AROUND BUCKET
    public void aroundBucket(){
        nextState = "";
        //goForward(1200);
        if (nextState == States.BARCODE_SCANNED) {
            setNextState(States.MOVEAWAY);
            safeSleep(500);
            setNextState(States.STOPPING);
            return;
        }
        //goForward(2800);
        sendCommand(Commands.STOP);
        safeSleep(200);
        sendCommand(Commands.LEFT);
        if (nextState == States.BARCODE_SCANNED) {
            setNextState(States.MOVEAWAY);
            safeSleep(500);
            setNextState(States.STOPPING);
            return;
        }
        safeSleep(999);
        if (nextState == States.BARCODE_SCANNED) {
            setNextState(States.MOVEAWAY);
            safeSleep(500);
            setNextState(States.STOPPING);
            return;
        }
        sendCommand(Commands.RIGHT);
        safeSleep(1500);
        sendCommand(Commands.STOP);
        safeSleep(300);
        sendCommand(Commands.RIGHT);



        if (nextState == "") {
            setNextState(States.END); // Use to be END, now it is find bucket.
        }
    }
//*****************************************************************************************************
    public void goForward(long milli) {
        sendCommand(Commands.FORWARD);
        long currentTime = System.currentTimeMillis();

        long timePassed = System.currentTimeMillis() - currentTime;
        while(timePassed < milli){
           // getNewLidarValue();
            getlastPing();
            timePassed = System.currentTimeMillis() - currentTime;
            if (mPingValue < 35 && mPingValue > 0){
                LogEx.d("Stopping Go Forward Because Ping is  " + mPingValue);
                avoidBucket();
            }
            LogEx.d("Time Passed:" + timePassed);
        }


    }
    //*****************************************************************
    //Snake Method
    public void snake() {
        LogEx.d("The Snake Made A Hiss");
        nextState = "";
    //******************************************Snake Movement

        int duration = 40;

        LogEx.d("FORWARD #1");
        goForward(duration * 1000);

        LogEx.d("RIGHT #1");
        sendCommand(Commands.RIGHT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #2");
        goForward(duration * 1000);

        LogEx.d("LEFT #1");
        sendCommand(Commands.LEFT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #3");
        goForward(duration * 1000);

        LogEx.d("RIGHT #2");
        sendCommand(Commands.RIGHT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #4");
        goForward(duration * 1000);

        LogEx.d("LEFT #2");
        sendCommand(Commands.LEFT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #1");
        goForward(duration * 1000);

        LogEx.d("RIGHT #1");
        sendCommand(Commands.RIGHT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #2");
        goForward(duration * 1000);

        LogEx.d("LEFT #1");
        sendCommand(Commands.LEFT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #3");
        goForward(duration * 1000);

        LogEx.d("RIGHT #2");
        sendCommand(Commands.RIGHT);
        safeSleep(5 * 1000);

        LogEx.d("FORWARD #4");
        goForward(duration * 1000);

        LogEx.d("LEFT #2");
        sendCommand(Commands.LEFT);
        safeSleep(5 * 1000);
    //******************************************
        if (nextState == "") {
            setNextState(States.END); // Use to be END, now it is find bucket.
        }
    }


    //*****************************************************************
    //NOTE// Spiral is being used as a test for now.
    public void spiral() {
        LogEx.d("Begin Spiral");
        goForward(3900);
        sendCommand(Commands.LEFT);
        safeSleep(1900);
        sendCommand(Commands.LEFT);
        safeSleep(400);
        goForward(2500);
        sendCommand(Commands.RIGHT);
        safeSleep(2000);
        goForward(1000);
        sendCommand(Commands.RIGHT);
        safeSleep(2000);
        sendCommand(Commands.LEFT);




    }

    //******************************************************************avoidBucket
        public void avoidBucket() {
            sendCommand(Commands.BACK);
            safeSleep(1500);
            sendCommand(Commands.STOP);
            safeSleep(300);
            sendCommand(Commands.LEFT);
            safeSleep(1320);
            sendCommand(Commands.RIGHT);
            safeSleep(1320);
            sendCommand(Commands.STOP);
            safeSleep(400);
            sendCommand(Commands.RIGHT);
            safeSleep(428);
            sendCommand(Commands.FORWARD);
            safeSleep(500);
        }
        //**********************************//
        //Avoid Bucket will go around an object in snake mode.
    //******************************************************************
//README: The Comp On Sunday May 31st is a area by 100ft
    //******************************************************************************************
    // Util functions
    //******************************************************************************************
    private void safeSleep(long milis){
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //******************************************************************************************
    // Bluetooth support
    //******************************************************************************************
    public void setupBluetooth(){
        bt  = new BluetoothSPP((Context)mActivity);

        bt.startService(BluetoothState.DEVICE_OTHER);

        if(!bt.isBluetoothEnabled()) {
            bt.enable();
        }

        if(!bt.isServiceAvailable()) {
            bt.setupService();
            bt.startService(BluetoothState.DEVICE_OTHER);
        }

        bt.setBluetoothConnectionListener(mBluetoothConnectionListener);
        bt.setOnDataReceivedListener(mDataReceiveListener);

        bt.connect("20:15:03:31:06:23");

    }
    public void bluetoothReconnect(){
        bt.disconnect();
        bt.connect("20:15:03:31:06:23");

    }

    private BluetoothSPP.BluetoothConnectionListener mBluetoothConnectionListener = new BluetoothSPP.BluetoothConnectionListener() {
        public void onDeviceConnected(String name, String address) {
            LogEx.d("Device connected" + name);
            Toast.makeText((Context)mActivity,"Bluetooth Connected",Toast.LENGTH_SHORT).show();
        }

        public void onDeviceDisconnected() {
            LogEx.d("Device disconnected");
            Toast.makeText((Context)mActivity,"Bluetooth Disconnected",Toast.LENGTH_SHORT).show();
        }

        public void onDeviceConnectionFailed() {
            LogEx.d("Connection failed");
            Toast.makeText((Context)mActivity,"Bluetooth Connection failed",Toast.LENGTH_SHORT).show();
        }
    };
 
    //this sends the command to arduino //H0-05 Bluetooth Device ID:  20:14:12:03:23:07 //H0-06 ID: 20:15:03:31:06:23
    public void sendCommand(String msg){
        bt.send(msg + ((char)13), false);
        safeSleep(500);
    }

    public String getLastResponse(){
        return lastReceivedMsg;
    }

    private String lastReceivedMsg = "";
    private BluetoothSPP.OnDataReceivedListener mDataReceiveListener = new BluetoothSPP.OnDataReceivedListener() {
        public void onDataReceived(byte[] bytes, String s) {
            lastReceivedMsg = s;
            LogEx.d("Data received: " + s);
            mActivity.onLogMessage(s);

            if (s.contains("LIDAR:")){
                mLidarValue = Integer.parseInt(s.split(":")[1]);
            }

            if (s.contains("PING:")){
                mPingValue = Integer.parseInt(s.split(":")[1]);
            }
        }
    };


    public static class Commands{
        public static final String FORWARD = "FORWARD";
        public static final String LIDAR = "LIDAR";
        public static final String BACK = "BACK";
        public static final String RIGHT = "RIGHT";
        public static final String LEFT = "LEFT";
        public static final String STOP = "STOP";
        public static final String PING = "PING";
    }


    public static class States{
        public static final String STOPPING = "STOPPING";
        public static final String STARTUP = "STARTUP";
        public static final String MOVEFORWARD = "MOVEFORWARD";
        public static final String MOVEBACK = "MOVEBACK";
        public static final String MOVELEFT = "MOVELEFT";
        public static final String MOVERIGHT = "MOVERIGHT";
        public static final String FIND_BUCKET = "FIND_BUCKET";
        public static final String BARCODE_SCANNED = "BARCODE_SCANNED" ;
        public static final String AROUND_BUCKET = "AROUND_BUCKET";
        public static final String MOVE_TO_BUCKET = "MOVE_TO_BUCKET";
        public static final String END = "END" ;
        public static final String MOVEAWAY = "MOVEAWAY";
        public static final String SPIRAL = "SPIRAL";
        public static final String SNAKE = "SNAKE";
    }

//******************************************************//Rescue Mode v2
    //Plan:
    //0.Set GPS HOME
    //1. Use Servo And Turn Camera To Look Around
    //2. Determine Object In Area EX: Defined By Color Or Movement.
    //3.Move To Object Avoiding Obstacles Around.
    //4. Move Around Object To Determine Status Then Save GPS
    //5.Return Home



    // PLAN2 - To Add
    //1. 360 Lidar
    //2. Serial Communication With Cable
    //3. Mapping VS Pattern. NO MORE SNAKE PATTERN!


    //What must be removed.
    // 1. Bluetooth - replace with cable to interface faster with the tango.
    // 2. Ship Lidar back to manufacturer. - Return new.
    // 3. Set Robots GPS home so if it has to return to the start it will.



//IDEAS//
    // Use the Motor to turn the project tango.
    // SLAM
    // New Wheels?
    // Tracking Constantly
    //
    //
    //
    //
    //
    //
    //
    //
    //







    public void gpsHome(){


    }

    public void lidarSpin() {


    }

    public void remoteControler(){
        //To test the chassi you can use a remote controller EX: Xbox controller.
    }

    public void safetyShutdown(){

    }

    public void tangoObjectRecognition(){
        //This will know if the object is in its veiw
    }

    public void avoidObject(){
        //Once tangoObjectRecognition is loaded then it will understand to avoid the object or objects.
    }

    public void bucketObjectRecognzie(){
        //This will look for the bucket shapes or other shapes that could be the bucket. Tango Dev Google+ has a video.
    }

    public void trytoScan(){
        //The robot will go around the bucket to try to see the QR code, if it sees it it will stop and it will continue finding the buckets
        //It will mark the bucket it has been to with maybe a digital mark to make sure it wont return to the same bucket.
    }

    public void markBucket(){
        //Marks the bucket to make sure it already has been there.
        //Or try to make it so it wont near the GPS cords but that requires the GPS location to be as close as it can to the bucket.
        //#Google# Digital mark for a bucket or an object.
    }

    public void returntoHome(){
        //As implied return to the start.
    }

    public void threesixtiyLidar(){

    }

    public void emergencyoverrideRoboclaw(){
        // will not let the roboclaw kill its self when it is stuck in grass.
    }

    public void detectBarrier(){
        //If to close to the barrier then the robot will back away.
        //if you find the barrier then head back to home or make a funcion called lastsafeLocation() and see if that works.
    }

    public void searchforSquare(){

    }

    public void setHome(){

    }

    




//******************************************************
}            //End Of Whole Robot Class

