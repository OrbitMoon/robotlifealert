package com.bdcsoftware.robotlifealert;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.Settings;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import app.akexorcist.bluetotohspp.library.BluetoothSPP;
import barcode.BarcodeCaptureActivity;
import barcode.core.Result;
import utils.LogEx;

public class TestActivity extends BarcodeCaptureActivity implements RobotActivity, LocationListener {
    BluetoothSPP bt;
    Spinner command;
    TextView responseText;
    Spinner states;
    Robot mRobot;
    LocationManager mLocationManager;
    String mLocationProvider;

    Location mCurrentLocation;

    final String barcodeLogPath = "robot_log.txt";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //before doing anything, verift GPS
        if (!verifyGPS()) {
            LogEx.d("NO GPS!!!!!!!!!!!!!!!!!!!!!");

        }

        //loud the screen layout
        setContentView(R.layout.test_activity);

        //create the robot object
        mRobot = new Robot(this);

        //setup the communications with arduino
        mRobot.setupBluetooth();

        //setup the buttons
        command = (Spinner) findViewById(R.id.command);
        responseText = (TextView) findViewById(R.id.responseText);
        states = (Spinner) findViewById(R.id.states);

        //add click handler to send button
        //this is used for debugging
        Button btnSend = (Button)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new OnClickListener(){
            public void onClick(View v){
                //send the command to the Robot (it will forward the command to Arduino)
                mRobot.sendCommand(command.getSelectedItem().toString());
            }
        });

        //add event handler to start button
        Button btnStartRobot = (Button) findViewById(R.id.btnStart);
        btnStartRobot.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //start the robot process
                mRobot.setNextState(Robot.States.STARTUP);
                mRobot.execute();
            }
        });

        Button btnConnect = (Button) findViewById(R.id.btnConnect);
        btnConnect.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //start the robot process
                LogEx.d("Connect Clicked");
                mRobot.bluetoothReconnect();
            }
        });

        final Button btnStates = (Button) findViewById(R.id.btnStates);
        btnStates.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                //start the robot process
                LogEx.d("Info Clicked");
                mRobot.setNextState(states.getSelectedItem().toString());
                mRobot.execute();
            }
        });

        //setup location manager
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        mLocationProvider = LocationManager.GPS_PROVIDER;
        mCurrentLocation = mLocationManager.getLastKnownLocation(mLocationProvider);
        if (mCurrentLocation != null){
            onLocationChanged(mCurrentLocation);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //start getting updates from location manager
        mLocationManager.requestLocationUpdates(mLocationProvider, 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //stop getting updates
        mLocationManager.removeUpdates(this);
    }

    //simple sleep function
    private void safeDelay(long milis){
        try {
            Thread.sleep(milis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //used to display messages from the robot
    public void onLogMessage(String msg) {
        responseText.setText("Msg: " + msg);
    }

    public void onLogCommand(final String cmd) {
        runOnUiThread(new Runnable() {
            public void run() {
                TextView tv = (TextView) findViewById(R.id.currentCommand);
                tv.setText(cmd);
            }
        });

    }

    @Override
    public void handleDecodeExternally(Result rawResult, Bitmap barcode) {
        LogEx.w("Barcode In Test Activity:" + rawResult);
        mRobot.setNextState(Robot.States.BARCODE_SCANNED);
        logBarcode(rawResult.toString());
    }


    //*****************************************************************************
    //GPS
    //*****************************************************************************
    private boolean verifyGPS(){
        LocationManager service = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = service
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        // check if enabled and if not send user to the GSP settings
        // Better solution would be to display a dialog and suggesting to
        // go to the settings
        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
            return false;
        }
        return true;
    }

    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        LogEx.d("Current location: " + location.getLatitude() + " " + location.getLongitude());
    }

    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {
        //dont do anything
    }

    public void onProviderDisabled(String provider) {
        //dont do anything
    }

    //*****************************************************************************
    //Logging to file
    //*****************************************************************************
    public void logBarcode(String barcode){
        File f = new File(Environment.getExternalStorageDirectory(), barcodeLogPath);
        try {

            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm:ss");
            Date resultdate = new Date(System.currentTimeMillis());

            FileWriter fw = new FileWriter(f, true);
            String value = "";
            if (mCurrentLocation != null) {
                value = "" + resultdate + "|" + barcode + "|" + mCurrentLocation.getLatitude() + "|" + mCurrentLocation.getLongitude() + "\r\n";
            } else {
                value = "" + resultdate + "|" + barcode + "||\r\n";
            }
            LogEx.d(value);
            fw.append(value);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
