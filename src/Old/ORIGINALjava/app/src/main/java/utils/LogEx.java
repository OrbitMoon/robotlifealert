package utils;

import android.util.Log;

/**
 * Creates a string from the provided Class, Method, and message and logs it
 * to the LogCat. The log tag is always "DroidLot"
 */
public class LogEx {
	private static String TAG = "RobotLifeAlert-Debug";
	
	
	public static void e(String theClass, String theMethod, String theMsg) {
		Log.e(TAG, theClass + "." + theMethod + " - " + theMsg);
	}
	
	public static void e(String theMsg) {
		Log.e(TAG, theMsg);
	}

	public static void i(String theClass, String theMethod, String theMsg) {
		Log.i(TAG, theClass + "." + theMethod + " - " + theMsg);
	}
	
	public static void i(String theMsg) {
		Log.i(TAG, theMsg);
	}

	public static void w(String theClass, String theMethod, String theMsg) {
		Log.w(TAG, theClass + "." + theMethod + " - " + theMsg);
	}
	
	public static void w(String theMsg) {
		Log.w(TAG, theMsg);
	}

	public static void v(String theClass, String theMethod, String theMsg) {
		Log.v(TAG, theClass + "." + theMethod + " - " + theMsg);
	}

	public static void d(String theClass, String theMethod, String theMsg) {
		Log.d(TAG, theClass + "." + theMethod + " - " + theMsg);
	}
	
	public static void d(String theMsg) {
		Log.d(TAG, theMsg);
	}
}
