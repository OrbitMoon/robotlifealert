package com.bdcsoftware.lifealert2;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.AsyncTask;
import android.util.Log;

import com.bdcsoftware.packetserial.PacketSerial;
import com.bdcsoftware.packetserial.SerialPortNotOpenException;
import com.bdcsoftware.packetserial.SimpleDualMotorPacket;

import java.io.IOException;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
/**
 * Created by dkfrayne on 1/17/16.
 */


public class Rover implements SensorEventListener, LocationListener{
    public static final int RIGHT_STOP = 192, LEFT_STOP = 64; //motor values
    private Azimuth azimuth; //compass bearing
    private boolean autonomous; //this being true is intended for overriding manual control with autonomous
    private PacketSerial packetSerial;
    private int leftMotor, rightMotor; //these need to belong strictly to Rover class if Rover is in charge of motion
    private double[] TranslationTango =new double[3];
    public static double latitude;
    public static double longitude;
    AutonomousAI autoAI;
    public PacketSerial getPacketSerial() {
        return packetSerial;
    }
    public void setPacketSerial(PacketSerial packetSerial) {
        this.packetSerial = packetSerial;
    }

    public double[] getTranslationTango() {
        return TranslationTango;
    }

    public void setTranslationTango(double[] translationTango) {
        TranslationTango = translationTango;
    }

    public Rover() {
        packetSerial = PacketSerial.getInstance();
        autonomous = false;
        leftMotor = LEFT_STOP;
        rightMotor = RIGHT_STOP;
        autoAI = new AutonomousAI();
    }

    public boolean isAutonomous() {
        return autonomous;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        try {
            azimuth.setDirection(Math.round(event.values[0]));
            //Log.d("SensorEvent", "value: " + azimuth.getDirection());
            //Log.d("SensorEvent", "compare(0)" + azimuth.compare(0));
        } catch (NullPointerException n) {
            azimuth = new Azimuth(Math.round(event.values[0]), true);
            //Log.d("SensorEvent", "value: " + azimuth.getDirection());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /*
     * this method is to be used only by other classes, specifically MainActivity, to run manual control
     */
    public void sendPayload(int leftMotor, int rightMotor) {
        if(!autonomous) {
            this.leftMotor = leftMotor;
            this.rightMotor = rightMotor;
            sendPayload();
        }
    }
    private void sendPayload() { //no motor arguments because the motors now belong to Rover class
        packetSerial.addPayloadPacket(new SimpleDualMotorPacket((byte) leftMotor, (byte) rightMotor));

        try {
            packetSerial.send();
        } catch (SerialPortNotOpenException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runAI() {
        autoAI.execute();}
    public void killAI() {
        autoAI.cancel(true);}

    protected class AutonomousAI extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            autonomous = false;
            Log.d("AutonomousAI", "Task finished");
        }
        @Override
        protected Void doInBackground(Void... params) {
            Log.d("AutonomousAI", "Snake method init");
            autonomous = true;
            snake3();
            return null;
        }
        /*
         * This method course corrects the robot auto-magically using the android compass
         * bearing and some clever geometry from the compareTo method of the azimuth class
         */
        private void updateMotorsWithTargetBearing(Azimuth targetDirection) {
            if(Math.abs(azimuth.compareTo(targetDirection)) > 5) {
                rightMotor += Math.round(azimuth.compareTo(targetDirection))/5;
                leftMotor -= Math.round(azimuth.compareTo(targetDirection))/5;
                sendPayload();
                Log.d("RoverLog", "updateMotors: L" + leftMotor + " R" + rightMotor);
            }
        }

        private void robotFaceDirection(Azimuth targetDirection) {
            while(Math.abs(azimuth.compareTo(targetDirection)) > 5) {
                rightMotor = RIGHT_STOP - 30 * azimuth.getRelativeSide(targetDirection);
                leftMotor = LEFT_STOP + 30 * azimuth.getRelativeSide(targetDirection);
                sendPayload();
                Log.d("RoverLog", "faceDirection: L" + (leftMotor - LEFT_STOP)
                        + " R" + (rightMotor - RIGHT_STOP));
                if(isCancelled()) break;
            }
        }

        /*
         * hopefully we'll get it right this time. the goal is to move in a square.
         */
        public void snake3() {
            long timeStart;
            Azimuth targetDirection = new Azimuth(azimuth.getDirection(), false);

            for(int i = 0; i < 4; i++) {
                timeStart = System.currentTimeMillis();

                rightMotor = RIGHT_STOP + 55;
                leftMotor = LEFT_STOP + 55;

                while(System.currentTimeMillis() < timeStart + 2000) {
                    updateMotorsWithTargetBearing(targetDirection);
                    if(isCancelled()) break;
                }

                rightMotor = RIGHT_STOP;
                leftMotor = LEFT_STOP;
                sendPayload();

                targetDirection.add(-90);
                robotFaceDirection(targetDirection);

                rightMotor = RIGHT_STOP;
                leftMotor = LEFT_STOP;
                sendPayload();
                if(isCancelled()) break;
            }
        }
        public void tangoSnake(){
            //get current tango distance
            //store in private var

            double startTango = TranslationTango[0];
            double currentTango = startTango;
            Log.d("TangoSnake", "Values Set");
            leftMotor = LEFT_STOP + 5;
            rightMotor = RIGHT_STOP + 5;
            //start motors forward
            sendPayload();

            while (currentTango-startTango < 2){
                currentTango = TranslationTango[0];
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            Log.d("TangoSnake", "You Moved 2 Meters");
            sendPayload(0, 0);
            Log.d("TangoSnake", "Ending Test! You Win!");

        }



    }

//    private double distance(double x1, double y1, double x0, double y0) {
//        return Math.sqrt(Math.pow(x0 - x1, 2) + Math.pow(y0 - y1, 2));
//    }




    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}


