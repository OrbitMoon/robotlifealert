package com.bdcsoftware.lifealert2;

import android.util.Log;

import com.bdcsoftware.packetserial.PacketSerial;
import com.bdcsoftware.packetserial.SerialPortNotOpenException;
import com.bdcsoftware.packetserial.SimpleDualMotorPacket;

import java.io.IOException;

/**
 * Created by dkfrayne on 1/17/16.
 */
//Saturday 13, 2016 Feb. *Denny Removed Multiple Commits so had to be fixed* Issue: Resolved

public class Rover extends BaseRover {
    private PacketSerial packetSerial;

    public Rover() {
        super();
        packetSerial = PacketSerial.getInstance();
    }


    /*
     * this method is to be used only by other classes, specifically MainActivity, to run manual control
     */
    public void sendPayload(int leftMotor, int rightMotor) {
        if(!isAutonomous()) {
            this.leftMotor = leftMotor;
            this.rightMotor = rightMotor;
            sendPayload();
        }
    }
    @Override
    public void sendPayload() {
        super.sendPayload();
        packetSerial.addPayloadPacket(new SimpleDualMotorPacket((byte) leftMotor, (byte) rightMotor));

        try {
            packetSerial.send();
            Log.d("Payload", "Payload sent.");

        } catch (SerialPortNotOpenException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * LOG REPORTS:
     */

    // [2/24/16] Log.

    // 2/24/16 RIP Denny... Roboclaw might have died.
    //To-Do:
    // Simplify the functions of the tango move, EX: movetango(Paramaters)
    // Robot won't move again during Tango-Snake code...
}


