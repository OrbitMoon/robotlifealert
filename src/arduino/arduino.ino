// Serial test script
#include <NewPing.h>
#include <I2C.h>
#include <Wire.h>
#define    LIDARLite_ADDRESS   0x62          // Default I2C Address of LIDAR-Lite.
#define    RegisterMeasure     0x00          // Register to write to initiate ranging.
#define    MeasureValue        0x04          // Value to initiate ranging.
#define    RegisterHighLowB    0x8f 
////////NEW PING
#define TRIGGER_PIN  12
#define ECHO_PIN     11
#define MAX_DISTANCE 200

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);

///////////////////////////////////
int setPoint = 55;
String readString;
//////////////////////////////////// VALUES For ROBOT
const int RS = 64;
const int LS = 192;
////////////////////////////////////Lidar
int lastLidarValue = 0;
int lastPing = 0;
////////////////////////////////////
char received_command[50];

void setup()
{

  Serial.begin(57600);  // initialize serial communications at 9600 bps
  Serial1.begin(9600);
  Serial3.begin(115200);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  ///////////////////////Lidar I2c
 //Serial.begin(9600); //Opens serial connection at 9600bps.     
 I2c.begin(); // Opens & joins the irc bus as master
 delay(100); // Waits to make sure everything is powered up before sending or receiving data  
 I2c.timeOut(50); // Sets a timeout to ensure no locking up of sketch if I2C communication fails
 

}

void loop()
{
 // readLidar();
 getPing();
 // Serial.println("A");
   if (getCommand()){
    Serial.println("Received command:");
    Serial.println(received_command);
    
    if (strcmp(received_command, "TEST") == 0){
      //need to send something back
      Serial.println("Sending back..");
      Serial3.println("99");
    }
    
    if (strcmp(received_command, "FORWARD") == 0){
      Forward(25);
    }
    
     if (strcmp(received_command, "BACK") == 0){
      Back(25);
    }
    
     if (strcmp(received_command, "RIGHT") == 0){
      turnRight(25);
    }
    
     if (strcmp(received_command, "LEFT") == 0){
      turnLeft(25);
    }
    
     if (strcmp(received_command, "LIDAR") == 0){
         Serial3.print("LIDAR:");Serial3.println(lastLidarValue);
    }
    
       if (strcmp(received_command, "STOP") == 0){
      Stop();
    }
    
      if (strcmp(received_command, "PING") == 0){
         Serial3.print("PING:");Serial3.println(lastPing);
    }
    
    
    wipeCommand();
  }
  //Lidar();
  //while(!Serial.available()) {}
 //  serial read section
//////////////////////////////////////////// OLD MOVEMENT FUNCTIONS
//  while (Serial.available())
//  {
//    readLidar();
//    if (Serial.available() >0)
//    {
//      char c = Serial.read();  //gets one byte from serial buffer
//    
//      if (c == 'F') {
//          //move forward
//          Serial.println("ACK");
//          Forward(25);
//        //  RCF(10000, 150);
//      } else if (c == 'B') { 
//        //move Back
//          Serial.println("ACK");
//          Back(25);
//        
//      } else if (c == 'L') { 
//        //move left
//          Serial.println("ACK");
//          turnLeft(25);
//      
//      } else if (c == 'R') {
//        //move right
//         Serial.println("ACK");
//         turnRight(25);
//        
//      } else if (c == 'E') {
//        //Example 1: This will do a basic square movement!
//         Serial.println("ACK EX1");
//         exampleOne();
//         
//      } else if (c == 'K'){
//        //Stop Arduino
//        Serial.println("ACK STOP ALL FUNCTIONS");
//        killArduino();
//      } else if (c == 'V'){
//        Serial.println("ACK Lidar Sent");
//        Serial.println(lastLidarValue);
//      } else if(c == 'S'){
//        Serial.println("ACK STOP");
//        Stop();
//      }
//      
//      
//      
//      else {
//        Serial.println("NCK");
//      }
//      Serial.flush();
//    }
//  }
////////////////////////////////////////////////////////////////////////Simple Send
// if (readString.length() >0)
// {
//   Serial.print("Arduino received: ");  
//   Serial.println(readString); //see what was received
// }
////////////////////////////////////////////////////////////////////////
 // delay(500);

  // serial write section

  //char ard_sends = '1';
 // Serial.print("Arduino sends: ");
  //Serial.println(ard_sends);
  //Serial.print("\n");
  //Serial.flush();
}




///////////////////////////////////////////////////////////////////

bool getCommand(){
 // Serial.println("A");
  if (Serial3.available() > 0)  
   {
    // Serial.println("B");
     if(Serial3.readBytesUntil(13, received_command, 25)) {
       return true;
     }
   }
}

void wipeCommand(){
  for( int i = 0; i < sizeof(received_command);  ++i )
   received_command[i] = (char)0;
}

////////////CAR FUNCTIONS//////////////////////////////////////////
void Forward(int spd) {
 Serial1.write(RS+spd);
 Serial1.write(LS+spd+2);
 //delay(5000);
 
 
}
////////////////////////////Go Back
void Back(int spd) {
 Serial1.write(RS-spd);
 Serial1.write(LS-spd); 
  //delay(5000);
  
}
////////////////////////////Stop
void Stop() {
 Serial1.write(0);
 
}
////////////////////////////Turn Right
void turnRight(int spd) {
  Serial1.write(RS+spd);
  Serial1.write(LS+spd+23);
  
}
////////////////////////////Turn Left
void turnLeft(int spd) {
  Serial1.write(RS+spd+23);
  Serial1.write(LS+spd);
  
}
////////////////////////////////////////////SPIRAL
//void spiral(int spd){
//  Serial1.write(RS+spd);
//  Serial1.write(LS+Spd);
//  
//}

//////////////////////////////////////////////////////
////////////////////////////Stop alltogether and keep at stop
////////STOP ALLTOGETHER////
void killArduino() {
  Stop();
 for(;;)
  ; 
}
////////////////////////////////////////////
/////////EXAMPEL 1: Square Movement/////////
void exampleOne() {
Forward(25);
delay(3000);
turnRight(16);
delay(2120);
Forward(25);
delay(3000);
turnRight(16);
delay(2120);
Forward(25);
delay(3000);
turnRight(16);
delay(2120);
Forward(25);
delay(3000);
Stop();
}

//////////////////////////////////////////////////////////////////////////////////////////////
//README!!
// THE RC MEANS ITS JUST RC MODE, SO IF RAF IS TESTING COMMUNICATION AT HOME OR AT SCHOOL WITH 
// THE RC CAR, IT WILL MOVE!!
/////////////////////////////////
// TO USE IT, IN FORWARD UNHASH [RCF] AND IT WILL MOVE!!
//////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////// RC CAR FORWARD TO TEST
void RCF(int duration, int speedDel) {
  int count = duration;
  while (count > 0){
    count = count - speedDel;
    digitalWrite(8, HIGH);
    digitalWrite(9, LOW);
  
  }
  
  
}
///////////////////////////////////////////////////////////////////////////////////////////////LIDAR STUFF
//////LIDAR//////
void readLidar(){
  // Write 0x04 to register 0x00
  uint8_t nackack = 100; // Setup variable to hold ACK/NACK resopnses     
  while (nackack != 0){ // While NACK keep going (i.e. continue polling until sucess message (ACK) is received )
    nackack = I2c.write(LIDARLite_ADDRESS,RegisterMeasure, MeasureValue); // Write 0x04 to 0x00
    delay(1); // Wait 1 ms to prevent overpolling
   //Serial.println("A");
}
  

  byte distanceArray[2]; // array to store distance bytes from read function
  
  // Read 2byte distance from register 0x8f
  nackack = 100; // Setup variable to hold ACK/NACK resopnses     
  while (nackack != 0){ // While NACK keep going (i.e. continue polling until sucess message (ACK) is received )
    nackack = I2c.read(LIDARLite_ADDRESS,RegisterHighLowB, 2, distanceArray); // Read 2 Bytes from LIDAR-Lite Address and store in array
    delay(1); // Wait 1 ms to prevent overpolling
    //Serial.println("B");
  }
   lastLidarValue = (distanceArray[0] << 8) + distanceArray[1];  // Shift high byte [0] 8 to the left and add low byte [1] to create 16-bit int
  //Serial.println("C");
  // Print Distance
  delay(100);
 // Serial.println(lastLidarValue);
 }
 ///////////////////////////////////////NEW PING CODE
 
void getPing() {
  delay(50);
  int uS = sonar.ping();
  lastPing = uS / US_ROUNDTRIP_CM;
  Serial.println(lastPing);
 }


