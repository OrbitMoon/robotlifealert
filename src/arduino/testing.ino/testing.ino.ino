char received_command[50];

void setup(){
  Serial3.begin(9600); 
  Serial.begin(9600);
  Serial.println("Exit setup");
}

void loop(){
  if (getCommand()){
    Serial.println("Received command:");
    Serial.println(received_command);
    if (strcmp(received_command, "TEST") == 0){
      //need to send something back
      Serial.println("Sending back..");
      Serial3.println("67");
    }
    wipeCommand();
  }
  delay(1000);
  Serial.println("loop...");
}

bool getCommand(){
  if (Serial3.available() > 0)  
   {
     if(Serial3.readBytesUntil(13, received_command, 25)) {
       return true;
     }
   }
}

void wipeCommand(){
  for( int i = 0; i < sizeof(received_command);  ++i )
   received_command[i] = (char)0;
}
