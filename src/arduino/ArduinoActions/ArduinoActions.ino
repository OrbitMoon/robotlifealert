// ReceivePackets.ino - Demo application to receive data and command messages
// Copyright 2012 Jeroen Doggen (jeroendoggen@gmail.com)
//
// Warning: this code is untested (it compiles, but not tested on hardware)

#include <PacketSerial.h>

uint8_t sensorID=56;
uint8_t payload=5;

const int RS = 64;
const int LS = 192;

PacketSerial packetSerial;

void setup()
{
  Serial1.begin(9600);
  Serial3.begin(115200);
  Serial3.println("");
  Serial3.println("Setup start[ArduinoActions]");
  // We must specify a packet handler method so that
  packetSerial.setPacketHandler(&onPacket);
  packetSerial.begin(115200, 0);
  Serial3.println("Setup finished[ArduinoActions]");
  pinMode(13, OUTPUT);
}

void loop()
{
  delay(500);

  packetSerial.update();
  }
  //update is and must be at end of loop!
  


/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
time loop() runs, so using delay inside loop can delay
response.  Multiple bytes of data may be available.
*/

void onPacket(const uint8_t* buffer, size_t size)
{
  Serial3.print("Received packet:");
  for (int i = 0; i < size; i++){
    Serial3.print(buffer[i], HEX);
  }
  Serial3.println("");

  if(buffer[0] == 0x01){
    //this is motor speed command
    //left motor is passed as byte[1]
    //right mottor is byte[2]
    Serial3.print("Left motor should be set to:");
    Serial3.print(buffer[1], HEX);
    Serial3.println("");
    Serial3.print("Right motor should be set to:");
    Serial3.print(buffer[2], HEX);
    Serial3.println("");
    setMotorSpeed(buffer[1], buffer[2]);
  }
  
}

void setMotorSpeed(int left, int right){
  Serial1.write(left);
  Serial1.write(right);
}

void stopBothMotors(){
  Serial1.write(0);
  Serial.println("MOTORS STOPPED!");
}

void Forward(int spd) {
 Serial1.write(RS+spd);
 Serial1.write(LS+spd+2);
 //delay(5000);
 
 
}
////////////////////////////Go Back
void Back(int spd) {
 Serial1.write(RS-spd);
 Serial1.write(LS-spd); 
  //delay(5000);
  
}
////////////////////////////Stop
void Stop() {
 Serial1.write(0);
 
}
////////////////////////////Turn Right
void wheelRight(int spd) {
  Serial1.write(RS+spd);
  //Serial1.write(LS+spd+23);
  
}

////////////////////////////Turn Left
void wheelLeft(int spd) {
  //Serial1.write(RS+spd+23);
  Serial1.write(LS+spd);
  
}


  
